using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PetShop.Data;
using PetShop.Models;

namespace PetShop.Repositories
{
    public class OwnerRepository : IOwnerRepository
    {
        private readonly Context _context;

        public OwnerRepository(Context context)
        {
            _context = context;
        }

        public async Task<Owner> AddAsync(Owner owner)
        {
            await _context.Owners.AddAsync(owner);
            await _context.SaveChangesAsync();

            return owner;
        }

        public async Task<List<Owner>> ListAsync()
        {
            return await _context.Owners.ToListAsync();
        }
    }
}