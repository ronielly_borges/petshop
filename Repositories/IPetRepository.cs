using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PetShop.Models;

namespace PetShop.Repositories
{
    public interface IPetRepository
    {
        Task<Pet> AddAsync(Pet pet);
        Task<List<Pet>> ToListAsync();
        Task<Pet> GetByIdAsync(int id);
        Task<List<Pet>> GetByNameAsync(string name);
        Task<List<Pet>> GetAllByIdOwner(int id);
        Task<bool> Update(Pet pet, int id);
    }
}