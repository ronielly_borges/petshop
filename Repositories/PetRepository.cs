using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PetShop.Data;
using PetShop.Models;

namespace PetShop.Repositories
{
    public class PetRepository : IPetRepository
    {
        private readonly Context _context;

        public PetRepository(Context context)
        {
            _context = context;
        }

        public async Task<Pet> AddAsync(Pet pet)
        {
            await _context.Pets.AddAsync(pet);
            await _context.SaveChangesAsync();

            return pet;
        }

        public async Task<Pet> GetByIdAsync(int id)
        {
            return await _context.Pets.AsNoTracking()
                                      .Include(x => x.Owner)
                                      .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<Pet>> GetAllByIdOwner(int id)
        {
            return await _context.Pets.AsNoTracking()
                                      .Include(x => x.Owner)
                                      .Where(x => x.OwenerId == id)
                                      .ToListAsync();
        }

        public async Task<List<Pet>> ToListAsync()
        {
            return await _context.Pets.ToListAsync();
        }

        public async Task<bool> Update(Pet pet, int id)
        {
            var petFind = await _context.Pets.FindAsync(id);
            
            if(petFind == null)
                return false;
                
            var petUpdated = pet.Update(pet);
            _context.Pets.Update(petUpdated);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<List<Pet>> GetByNameAsync(string name)
        {
            var pets = await _context.Pets.Where(pet => pet.Name == name).ToListAsync();

            return pets;
        }
    }
}