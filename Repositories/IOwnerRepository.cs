using System.Collections.Generic;
using System.Threading.Tasks;
using PetShop.Models;

namespace PetShop.Repositories
{
    public interface IOwnerRepository
    {
        Task<Owner> AddAsync(Owner owner);
        Task<List<Owner>> ListAsync();
    }
}