namespace PetShop.Enum
{
    public enum StatusCurrentHealth
    {
        InTreament = 1,
        Recovering = 2,
        Recovered = 3
    }
}