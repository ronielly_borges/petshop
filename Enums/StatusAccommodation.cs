namespace PetShop.Enum
{
    public enum StatusAccommodation
    {
        Busy = 1,
        Free = 2,
        WaitingOwner = 3
    }
}