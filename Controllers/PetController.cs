using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PetShop.Models;
using PetShop.Repositories;

namespace PetShop.Controllers
{
    [ApiController]
    [Route("v1/pets")]
    public class PetController : ControllerBase
    {
        [HttpPost]
        [Route("")]
        public async Task<ActionResult<Pet>> Post([FromServices]IPetRepository petRepository,
                                                  [FromBody] Pet model)
        {
            try
            {
                var pet = await petRepository.AddAsync(model);
                
                return Ok(pet);
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
        }
        
        [HttpGet]
        [Route("")]
        public async Task<ActionResult<List<Pet>>> Get([FromServices]IPetRepository petRepository)
        {
            try
            {
                var pets = await petRepository.ToListAsync();

                return pets;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        [HttpGet]
        [Route("{id:int}")]
        public async Task<ActionResult<Pet>> GetByIdPet([FromServices]IPetRepository petRepository, int id)
        {
            try
            {
                var pet = await petRepository.GetByIdAsync(id);
                if(pet == null)
                    return NotFound(id);
                
                return Ok(pet);
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("owners/{id:int}")]
        public async Task<ActionResult<List<Pet>>> GetByIdOwner([FromServices]IPetRepository petRepository, int id)
        {
            try
            {
                var pets = await petRepository.GetAllByIdOwner(id);
                if(pets == null)
                    return NotFound();
                
                return Ok(pets);
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> Put([FromServices]IPetRepository petRepository, [FromBody]Pet model, int id)
        {
            try
            {
                var isUpdate = await petRepository.Update(model, id);
                if(!isUpdate)
                    NotFound();

                return Ok();
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("{name}")]
        public async Task<IActionResult> GetByName([FromServices]IPetRepository petRepository, string name)
        {
            try
            {
                var pets = await petRepository.GetByNameAsync(name);
                if(pets.Count == 0)
                    return NotFound();
                
                return Ok(pets);
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}