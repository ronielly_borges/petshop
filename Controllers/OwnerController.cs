using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PetShop.Data;
using PetShop.Models;
using PetShop.Repositories;

namespace PetShop.Controllers
{
    [ApiController]
    [Route("v1/owners")]
    public class OwnerController : ControllerBase
    {
        [HttpPost]
        [Route("")]
        public async Task<ActionResult<Owner>> Post([FromServices]IOwnerRepository ownerRepository, [FromBody]Owner model)
        {
            //Validate model
            try
            {
                await ownerRepository.AddAsync(model);
                
                return Ok(model);
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("")]
        public async Task<ActionResult<List<Owner>>> Get([FromServices]IOwnerRepository ownerRepository)
        {
            try
            {
                var owners = await ownerRepository.ListAsync();
                return owners;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
    }
}