using System.Collections;

namespace PetShop.Models
{
    public class Owner
    {
        public Owner(int id, string name, string address, string telephone)
        {
            Id = id;
            Name = name;
            Address = address;
            Telephone = telephone;
        }

        public int Id { get; private set; }
        public string Name { get; private set; }
        public string Address { get; private set; }
        public string Telephone { get; private set; }
    }
}