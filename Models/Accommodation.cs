using PetShop.Enum;

namespace PetShop.Models
{
    public class Accommodation
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public StatusAccommodation StatusAccommodation { get; set; }
    }
}