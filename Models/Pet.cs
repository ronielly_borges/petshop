using PetShop.Enum;

namespace PetShop.Models
{
    public class Pet
    {
        public Pet(int id, string name, string reasonHospitalization, StatusCurrentHealth statusCurrentHealth, string photograph, int owenerId, Accommodation accommodation)
        {
            Id = id;
            Name = name;
            ReasonHospitalization = reasonHospitalization;
            StatusCurrentHealth = statusCurrentHealth;
            Photograph = photograph;
            OwenerId = owenerId;
            Accommodation = accommodation;
        }

        public Pet Update(Pet pet)
        {
            Name = pet.Name;
            ReasonHospitalization = pet.ReasonHospitalization;
            StatusCurrentHealth = pet.StatusCurrentHealth;
            Photograph = pet.Photograph;

            return this;
        }

        public int Id { get; private set; }
        public string Name { get; private set; }
        public string ReasonHospitalization { get; private set; }
        public StatusCurrentHealth StatusCurrentHealth { get; private set; }
        public string Photograph { get; private set; }
        public int OwenerId { get; private set; }
        public virtual Owner Owner { get; private set; }
        public Accommodation Accommodation { get; private set; }
    }
}